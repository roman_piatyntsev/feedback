jQuery().ready(function ($) {
    /**
     * Form submit Hundler.
     * Send form data via AJAX to the site. 
     */
    $(".feedback-form").submit(function ajaxSubmit(e) {
        e.preventDefault();
        var form_wrapper = $(this).parents(".feedback-form-holder");
        var feedbackForm = $(this).serializeArray();

        feedbackForm.push({
            name: "action",
            value: "add_feedback"
        });

        $.ajax({
            type: "POST",
            url: feedback_ajax.url,
            data: $.param(feedbackForm),
            beforeSend: () => {
                $(".error-message", this).remove(); // delete all error messages
                $(".submit", this).addClass("feedback-ajax-doing");
            },
            success: (result) => {
                if (result.success === true) { // feedback added successfuly
                    $(this).addClass("submitted");
                    this.reset();
                } else { // feedback validate failed
                    for (var field_name in result.data) {
                        let error_message = result.data[field_name];

                        // Get element by name
                        let element = $("[name='" + field_name + "']", this);

                        // Set error class of form control wrapper
                        element.parents(".feedback-form-control-wrap").addClass("invalid");
                        $("<span class='error-message'>" + error_message + "</span>").insertAfter(element);
                    }
                }
                // reset total error message
                $(".feedback-error", form_wrapper).text('');
            },
            error: () => {
                // set total error message on the bottom of the form
                $(".feedback-error", form_wrapper).text(feedback_ajax.unknow_error_message);
            },
            complete: () => {
                $(".submit", this).removeClass("feedback-ajax-doing");
            }
        });
        return false;
    });
});