jQuery().ready(function ($) {

    var entries = {}; // to save feedbacks ID and messages from AJAX.

    /**
     * AJAX paginations.
     */
    $(".feedback-list-wrapper").on('click', ".feedback-paginate a", function (el) {
        el.preventDefault();

        let page = $(this).attr('href').match(/[0-9]/g);

        if (page !== null) {
            page = page[0]; // get first index from match 
        } else {
            page = 1; // for the first page
        }

        if (page !== null) {

            // prepare data to ajax request
            let data = {
                action: "get_feedbacks_list",
                page: page,
            }
            // add nonce action and value
            data[feedback_list_ajax.get_feedback_action] = feedback_list_ajax.get_feedback_nonce;

            $.ajax({
                type: "POST",
                url: feedback_list_ajax.url,
                data: data,
                beforeSend: () => {
                    $(".feedback-list-error", ".feedback-list-wrapper").text('');
                    $(".feedback-list-wrapper").addClass("feedback-ajax-doing");
                },
                success: (result) => {
                    if (result.success === true) {
                        $(".feedback-body").html(result.data);
                        scroll_to("#feedback-list-id", 50);
                    }
                },
                error: () => {
                    // set total error message on the bottom of the list
                    $(".feedback-list-error", ".feedback-list-wrapper").text(feedback_list_ajax.unknow_error_message);
                },
                complete: () => {
                    $(".feedback-list-wrapper").removeClass("feedback-ajax-doing");
                }
            });
        }

    });

    /**
     * AJAX get a single feedback.
     */
    $(".feedback-list-wrapper").on('click', ".feedback-entrie", function (el) {

        // get feedback ID from data attribures.
        feedback_id = $(this).data("feedback");

        // check if data attribite is exist.
        if (typeof feedback_id !== 'undefined') {

            // clear selected field
            $(".feedback-entrie").removeClass('feedback-selected');

            // is feedback message downloaded already?
            // check if ID is exist in object.
            if (entries.hasOwnProperty(feedback_id)) {

                // set current feedback to 'selected_feedback' object.
                set_current_feedback_value(feedback_id);
                $(this).addClass('feedback-selected');
                scroll_to("#current-feedback-id", 50);

            } else { // download message via AJAX.

                // prepare data to ajax request.
                let data = {
                    action: "get_feedback",
                    feedback_id: feedback_id,
                }
                // add nonce action and value.
                data[feedback_list_ajax.get_feedback_action] = feedback_list_ajax.get_feedback_nonce;

                $.ajax({
                    type: "POST",
                    url: feedback_list_ajax.url,
                    data: data,
                    beforeSend: () => {
                        // reset all errors
                        $(".feedback-list-error", ".feedback-list-wrapper").text('');
                        $(this).addClass("feedback-ajax-doing");
                    },
                    success: (result) => {
                        if (result.success === true) {
                            // save message according to feedback ID
                            entries[feedback_id] = result.data;

                            // set current feedback to 'selected_feedback' object
                            set_current_feedback_value(feedback_id);
                            scroll_to("#current-feedback-id", 50);
                            $(this).addClass("feedback-selected");
                        }
                    },
                    error: () => {
                        // clear selected field
                        $(".feedback-entrie").removeClass('feedback-selected');

                        // set total error message on the bottom of the list
                        $(".feedback-list-error", ".feedback-list-wrapper").text(feedback_list_ajax.unknow_error_message);
                    },
                    complete: () => {
                        $(this).removeClass("feedback-ajax-doing");
                    }
                });
            }
        }

        return false;
    });

    function scroll_to(el, offset) {
        $('html, body').animate({
            scrollTop: $(el).offset().top - offset
        }, 500);
    }

    /**
     * Set data of the current (selected) feedback.
     */
    function set_current_feedback_value(feedback_id) {
        fedback_fileds = [
            'feedback-first-name',
            'feedback-last-name',
            'feedback-email',
            'feedback-subject'
        ];

        // source to copy data to the current selected feeback 
        let source = $('[data-feedback="' + feedback_id + '"]');

        fedback_fileds.forEach(function (el) {
            let text_from = $("." + el, source).text();
            $("." + el, '#current-feedback-id').text(text_from);
        });
        $(".feedback-message", "#current-feedback-id").text(entries[feedback_id]);
        $("#current-feedback-id").addClass("selected");
    }
});