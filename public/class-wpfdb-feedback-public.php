<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines hooks to enqueue the public-facing stylesheet and JavaScript.
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */
class WPFDB_Feedback_Public {

	/**
	 * Enqueue the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * Enqueue the main styles for the public-facing side of the site.
		 */
		wp_enqueue_style(
			WPFDB_PLUGIN_NAME,
			plugin_dir_url( __FILE__ ) . 'css/feedback-styles.css',
			array(),
			WPFDB_PLUGIN_VERSION
		);
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function register_scripts() {

		/**
		 * Register the scripts for the feedback form.
		 */
		wp_register_script(
			WPFDB_PLUGIN_NAME,
			plugin_dir_url( __FILE__ ) . 'js/feedback-form.js',
			array( 'jquery' ),
			WPFDB_PLUGIN_VERSION,
			true
		);

		/**
		 * Register global variables.
		 */
		wp_localize_script(
			WPFDB_PLUGIN_NAME,
			'feedback_ajax',
			array(
				'url'                  => admin_url( 'admin-ajax.php' ),
				'unknow_error_message' => esc_html__( 'Sorry, can not send the feedback. Try again later.', 'wp-feedback' ),
			)
		);

		/**
		 * Register the scripts for the feedbacks list.
		 */
		wp_register_script(
			WPFDB_PLUGIN_NAME . '-list',
			plugin_dir_url( __FILE__ ) . 'js/feedback-list.js',
			array( 'jquery' ),
			WPFDB_PLUGIN_VERSION,
			true
		);

		/**
		 * Register global variables.
		 */
		wp_localize_script(
			WPFDB_PLUGIN_NAME . '-list',
			'feedback_list_ajax',
			array(
				'url'                  => admin_url( 'admin-ajax.php' ),
				'unknow_error_message' => esc_html( __( 'Sorry, can not get the feedbacks. Try again later.', 'wp-feedback' ) ),
				'get_feedback_nonce'   => WPFDB_Feedback_Nonce::get_nonce_get_feedback(),
				'get_feedback_action'  => WPFDB_Feedback_Nonce::$nonce_get_feedback,
			)
		);
	}

	/**
	 * Print the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function print_scripts() {

		if ( WPFDB_Shortcode_Feedback_Form::$add_script ) {
			wp_enqueue_script( WPFDB_PLUGIN_NAME );
		}

		if ( WPFDB_Shortcode_Feedback_List::$add_script ) {
			wp_enqueue_script( WPFDB_PLUGIN_NAME . '-list' );
		}
	}
}
