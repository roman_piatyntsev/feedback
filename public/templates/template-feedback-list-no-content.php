<?php
/**
 * No feedbacks template of feedbacks list.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

<div class="no-feedbacks">
	<?php esc_html_e( 'No feedback yet', 'wp-feedback' ); ?>
</div>
