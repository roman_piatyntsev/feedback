<?php
/**
 * Acces denail page template.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>
</head>
<body class="feedback-denail-page">
	<div class="feedback-access-denail">
		<?php esc_html_e( 'You are not authorized to view the content of this page.', 'wp-feedback' ); ?>
	</div>
</body>
</html>
