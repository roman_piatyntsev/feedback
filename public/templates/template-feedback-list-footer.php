<?php
/**
 * Footer template of feedbacks list.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

	</div> <!-- //feedback-body -->
	<div class="feedback-list-error"></div>
</div> <!-- //feedback-list-wrapper -->
