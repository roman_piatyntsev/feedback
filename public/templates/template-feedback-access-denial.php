<?php
/**
 * Acces denail template.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

<div class="feedback-access-denail">
	<?php esc_html_e( 'You are not authorized to view the content.', 'wp-feedback' ); ?>
</div>
