<?php
/**
 * Header template of feedbacks list.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;
?>

<div id="feedback-list-id" class="feedback-list-wrapper">
	<div class="feedback-spinner"></div>
	<div class="feedback-row feedback-header">
		<div class="feedback-col feedback-first-name"><?php esc_html_e( 'First Name', 'wp-feedback' ); ?></div>
		<div class="feedback-col feedback-last-name"><?php esc_html_e( 'Last Name', 'wp-feedback' ); ?></div>
		<div class="feedback-col feedback-email"><?php esc_html_e( 'Email', 'wp-feedback' ); ?></div>
		<div class="feedback-col feedback-subject"><?php esc_html_e( 'Subject', 'wp-feedback' ); ?></div>
	</div>
	<div class="feedback-body">

