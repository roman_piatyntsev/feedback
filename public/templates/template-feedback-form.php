<?php
/**
 * The "feedback" form template.
 * This template using for the form to submit feedback to a site.
 *
 * $form_data is external data, see /includes/class-wpfdb-feedback-form.php
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

<div class="feedback-form-holder">
	<form action="#" method="post" class="feedback-form">
		<h3 class="feedback-form-title">
			<?php echo esc_html( $form_data['form-title'] ); ?>
		</h3>

		<?php
		/**
		 * Add nonce fiield to the form
		 */
		WPFDB_Feedback_Nonce::the_nonce_add_feedback();
		?>

		<p class="feedback-form-control-wrap first-name">
			<label><?php esc_html_e( 'First Name', 'wp-feedback' ); ?></label>
			<input type="text" required maxlength="60" name="feedback-first-name" class="feedback-form-control feedback-text feedback-require" value="<?php echo esc_attr( $form_data['first-name'] ); ?>">
		</p>

		<p class="feedback-form-control-wrap last-name">
			<label><?php esc_html_e( 'Last Name', 'wp-feedback' ); ?></label>
			<input type="text" required maxlength="60" name="feedback-last-name" class="feedback-form-control feedback-text feedback-require" value="<?php echo esc_attr( $form_data['last-name'] ); ?>">
		</p>

		<p class="feedback-form-control-wrap email">
			<label><?php esc_html_e( 'Email', 'wp-feedback' ); ?></label>
			<input type="email" required name="feedback-email" class="feedback-form-control feedback-email feedback-require" value="<?php echo esc_attr( $form_data['email'] ); ?>">
		</p>

		<p class="feedback-form-control-wrap subject">
			<label><?php esc_html_e( 'Subject', 'wp-feedback' ); ?></label>
			<input type="text" required maxlength="255" name="feedback-subject" class="feedback-form-control feedback-text feedback-require">
		</p>

		<p class="feedback-form-control-wrap message">
			<label><?php esc_html_e( 'Message', 'wp-feedback' ); ?></label>
			<textarea name="feedback-message" required maxlength="5000" cols="30" rows="5" class="feedback-form-control feedback-textarea feedback-require"></textarea>
		</p>

		<p class="feedback-form-control-wrap submit">
			<input type="submit" required value="<?php esc_attr_e( 'Submit Feedback', 'wp-feedback' ); ?>" class="feedback-button feedback-submit">
			<span class="feedback-spinner"></span>
		</p>
	</form>
	<div class="feedback-success">
		<span class="message"><?php echo esc_html( $form_data['thankyou-msg'] ); ?></span>
		<img src="<?php echo esc_url_raw( plugin_dir_url( dirname( __FILE__ ) ) . 'images/ok.png' ); ?>" alt="submitted sussess">
	</div>
	<div class="feedback-error"></div>
</div>
