<?php
/**
 * Template of single row of feedbaqck list.
 * Using in the loop to get feedback.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

<div class="feedback-row feedback-entrie" data-feedback="<?php the_ID(); ?>">
	<div class="feedback-spinner"></div>
	<div class="feedback-col feedback-first-name"><?php echo esc_html( $post_meta_data['first-name'] ); ?></div>
	<div class="feedback-col feedback-last-name"><?php echo esc_html( $post_meta_data['last-name'] ); ?></div>
	<div class="feedback-col feedback-email"><?php echo esc_html( $post_meta_data['email'] ); ?></div>
	<div class="feedback-col feedback-subject"><?php the_title(); ?></div>
</div>
