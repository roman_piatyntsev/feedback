<?php
/**
 * Template of selected feedback.
 *
 * @package Feedback
 * @subpackage Feedback/public/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

<div id="current-feedback-id" class="current-feedback">
	<div class="feedback-field feedback-user-meta">
		<div class="feedback-value feedback-first-name"></div>
		<div class="feedback-value feedback-last-name"></div>
		<div class="feedback-value feedback-email"></div>
	</div>
	<div class="feedback-field">
		<div class="feedback-label"><?php esc_html_e( 'Subject', 'wp-feedback' ); ?>:</div>
		<div class="feedback-value feedback-subject"></div>
	</div>
	<div class="feedback-field">
		<div class="feedback-label"><?php esc_html_e( 'Message', 'wp-feedback' ); ?>:</div>
		<div class="feedback-value feedback-message"></div>
	</div>
</div>
