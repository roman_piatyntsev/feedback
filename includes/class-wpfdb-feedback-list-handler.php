<?php
/**
 * The functionality of a list of all feedbacks.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The list of all feedbacks handler.
 *
 * The functionality of a list of all feedbacks.
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */
class WPFDB_Feedback_List_Handler {

	/**
	 * Hold the class instance.
	 *
	 * @var WPFDB_Feedback_List_Handler $instance Instance of a class
	 */
	private static $instance = null;

	/**
	 * The constructor is private
	 * to prevent initiation with outer code.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {
	}

	/**
	 * Get instance of class.
	 * Singlton.
	 *
	 * @since    1.0.0
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new WPFDB_Feedback_List_Handler();
		}
		return self::$instance;
	}

	/**
	 * Get only feedback entries.
	 * Using on AJAX request.
	 *
	 * @since    1.0.0
	 *
	 * @param    int $page     Number of a page.
	 */
	public function print_list_inner( $page = 1 ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return $this->print_access_denial();
		}

		$query = new WP_Query(
			array(
				'post_type'      => 'feedback',
				'posts_per_page' => 10,
				'paged'          => $page,
			)
		);

		ob_start();

		if ( $query->have_posts() ) {
			?>
			<div class="feedback-entries">
			<?php

			while ( $query->have_posts() ) {
				$query->the_post();

				/**
				 * Init user-meta to use in content template.
				 */
				$post_meta_data = array(
					'first-name'    => get_post_meta( get_the_ID(), '_wpfdb_meta_first_name', true ),
					'last-name'     => get_post_meta( get_the_ID(), '_wpfdb_meta_last_name', true ),
					'email'         => get_post_meta( get_the_ID(), '_wpfdb_meta_email', true ),
				);

				/**
				 * Require feedback content template.
				 */
				require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-list-content.php';

			}

			wp_reset_postdata();

			?>

			</div>

			<?php

		} else {

			/**
			 * Require no feedback template.
			 */
			require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-list-no-content.php';
		}

		/**
		 * Require current selected feedback template.
		 */
		require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-list-current.php';

		$paginate_links = paginate_links(
			array(
				'base'    => '%_%',
				'format'  => '?page=%#%',
				'current' => max( 1, $page ),
				'total'   => $query->max_num_pages,
			)
		);

		if ( $paginate_links ) {
			printf( '<div class="paginate feedback-paginate">%s</div>', $paginate_links );
		}

		return ob_get_clean();
	}

	/**
	 * Get list of all feedback.
	 *
	 * @since    1.0.0
	 *
	 * @param    int $page     Number of a page.
	 */
	public function print_list( $page = 1 ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return $this->print_access_denial();
		}

		ob_start();

		/**
		 * Require header of feedbacks list.
		 */
		require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-list-header.php';

		/**
		 * Require content of feedbacks list.
		 */
		echo $this->print_list_inner( $page );

		/**
		 * Require footer of feedbacks list.
		 */
		require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-list-footer.php';

		return ob_get_clean();
	}


	/**
	 * Unauthorized request message.
	 *
	 * @since    1.0.0
	 */
	public function print_access_denial() {

		ob_start();

		/**
		 * Unauthorized request template.
		 */
		require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-access-denial.php';

		return ob_get_clean();
	}

	/**
	 * Prevent access to the page with a list of feedbacks to an unauthorized user.
	 *
	 * @since    1.0.0
	 * 
	 * @param    string $original_template     Path to page template.
	 */
	public function template_include( $original_template ) {

		global $post;

		if ( $post !== null && has_shortcode( $post->post_content, 'feedback-list' ) && ! current_user_can( 'administrator' ) ) {
			return WPFDB_PLUGIN_PATH . '/public/templates/template-feedback-access-denial-page.php';
		} else {
			return $original_template;
		}
	}
}
