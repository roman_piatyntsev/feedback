<?php
/**
 * Store and handle current user.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Store and handle current user.
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Feedback_User {

	/**
	 * Current user
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      WP_User $current_user    Current user.
	 */
	private $current_user;

	/**
	 * Hold the class instance.
	 *
	 * @var WPFDB_Feedback_User $instance Instance of class.
	 */
	private static $instance = null;

	/**
	 * The constructor is private
	 * to prevent initiation with outer code.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {
	}

	/**
	 * Get instance of class.
	 * Singlton.
	 *
	 * @since    1.0.0
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new WPFDB_Feedback_User();
		}
		return self::$instance;
	}

	/**
	 * Init current user
	 *
	 * @since    1.0.0
	 */
	public function init_current_user() {
		$this->current_user = wp_get_current_user();
	}

	/**
	 * Get user meta data to fit feedback form filed.
	 *
	 * @since    1.0.0
	 */
	public function get_user_data() {

		/**
		 * Check if user loggedin and get email.
		 */
		$email = '';
		if ( is_user_logged_in() ) {
			$email = $this->current_user->user_email;
		}

		return array(
			'first-name' => get_user_meta( $this->get_current_user_id(), 'first_name', true ),
			'last-name'  => get_user_meta( $this->get_current_user_id(), 'last_name', true ),
			'email'      => $email,
		);
	}

	/**
	 * Return user ID or 0 if user not loggedin.
	 *
	 * @since    1.0.0
	 */
	public function get_current_user_id() {
		return get_current_user_id();
	}
}
