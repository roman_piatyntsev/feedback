<?php
/**
 * Manage administrator roles
 *
 * @since 1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/includes
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Manage administrator role.
 * Add capabilityes to manage 'feedback' post type.
 *
 * This is used to define AJAX wp-nonce fields.
 *
 * @since       1.0.0
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Customize_Admin_Role {

	/**
	 * List of plugin-related capabilities to add to the Administrator role.
	 *
	 * @since    1.0.0
	 * @var      array $caps    Contains all necessary capabilities.
	 */
	public static $caps = array(
		'edit_feedback',
		'read_feedback',
		'delete_feedback',
		'edit_feedbacks',
		'edit_others_feedbacks',
		'delete_feedbacks',
		'publish_feedbacks',
		'read_private_feedbacks',
		'delete_private_feedbacks',
		'delete_published_feedbacks',
		'delete_others_feedbacks',
		'edit_private_feedbacks',
		'edit_published_feedbacks',
		'edit_feedbacks',
	);

	/**
	 * Add feedback capability to administrator role.
	 *
	 * @since 1.0.0.
	 */
	public static function activate() {

		// get the Admin role's object from WP_Role class.
		$admin = get_role( 'administrator' );

		// add feedback capabilities.
		foreach ( self::$caps as $cap ) {
			$admin->add_cap( $cap );
		}
	}

	/**
	 * Remove feedback capability from administrator role.
	 *
	 * @since 1.0.0.
	 */
	public static function deactivate() {

		// get the Admin role's object from WP_Role class.
		$admin = get_role( 'administrator' );

		// revove feedback capabilities.
		foreach ( self::$caps as $cap ) {
			$admin->remove_cap( $cap );
		}
	}
}
