<?php
/**
 * Register plugin's AJAX handlers.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register plugin's AJAX handlers.
 *
 * Class registering three methods to handler next AJAX actions:
 * 1. 'get_feedback' - to get a single feedback for only admin roles;
 * 2. 'get_feedbacks_list' - to get a list of feedbacks for only admin roles;;
 * 3. 'add_feedback' - to save a new feedback;
 *
 * All of methods using for registered users and not registered users.
 *
 * @package     Feedback
 * @subpackage  Feedback/ajax
 */
class WPFDB_Feedback_Ajax {

	/**
	 * The array that contains all registered AJAX actions.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array $ajax_actions   Array of ajax actions.
	 */
	private $ajax_actions;


	/**
	 * Init instance of AJAX request hundlers.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function __construct() {
		$this->ajax_actions = array(
			'get_feedback',
			'get_feedbacks_list',
			'add_feedback',
		);
	}

	/**
	 * Get list of registered AJAX actions.
	 *
	 * @since   1.0.0
	 * @return  array   Array of registered AJAX actions.
	 */
	public function get_actions() {
		return $this->ajax_actions;
	}

	/**
	 * AJAX hundler of 'get_feedback' action.
	 * Get single feedback by id.
	 *
	 * @since   1.0.0
	 *
	 * @return  WP_Post|null  Object WP_Post or NULL if not found.
	 */
	public function get_feedback() {
		/**
		 * Check wp-nonce field;
		 */
		if ( ! WPFDB_Feedback_Nonce::verify_nonce_get_feedback() ) {
			wp_send_json_error( null, 403 );
		}

		$feedback = false;

		if ( isset( $_POST['feedback_id'] ) && is_numeric( $_POST['feedback_id'] ) ) {
			$feedback_id = intval( $_POST['feedback_id'] );
			$feedback    = get_post( $feedback_id, 'OBJECT', 'display' );
		}

		if ( $feedback && current_user_can( 'administrator' ) ) {
			// send content && status success.
			wp_send_json_success( $feedback->post_content, 200 );
		} else {
			// send status error.
			wp_send_json_error( null, 400 );
		}
	}

	/**
	 * AJAX hundler of 'get_feedbacks_list' action.
	 * Get list of feedbacks.
	 *
	 * @since   1.0.0
	 *
	 * @return  WP_Post[]|array()            Array of WP_Post objects or empty arrat if not found.
	 */
	public function get_feedbacks_list() {

		/**
		 * Check wp-nonce field;
		 */
		if ( ! WPFDB_Feedback_Nonce::verify_nonce_get_feedback() ) {
			wp_send_json_error( null, 403 );
		}

		$page = 1;

		if ( isset( $_POST['page'] ) && is_numeric( $_POST['page'] ) ) {
			$page = intval( $_POST['page'] );
		}

		$list_hundler = WPFDB_Feedback_List_Handler::get_instance();

		/**
		 * Return comtent only user with administrator roles.
		 * See includes/class-wpfdb-feedback-list-handler.php
		 */
		$feedback_list_html = $list_hundler->print_list_inner( $page );

		wp_send_json_success( $feedback_list_html );
	}

	/**
	 * AJAX hundler of 'add_feedback' action.
	 * Add feedback.
	 *
	 * @since   1.0.0
	 * @return  WP_Post[]|array()    Array of WP_Post objects or empty arrat if not found.
	 */
	public function add_feedback() {

		/**
		 * Validate and sanitize POST data.
		 */
		$form_handler      = WPFDB_Feedback_Form_Handler::get_instance();
		$validated_filelds = $form_handler->form_validate();

		if ( ! is_wp_error( $validated_filelds ) ) { // success.

			$post_data = array(
				'post_author'    => get_current_user_id(),
				'post_content'   => $validated_filelds['feedback-message'],
				'post_status'    => 'publish',
				'post_title'     => $validated_filelds['feedback-subject'],
				'post_type'      => 'feedback',
				'meta_input'     => array(
					'_wpfdb_meta_first_name' => $validated_filelds['feedback-first-name'],
					'_wpfdb_meta_last_name'  => $validated_filelds['feedback-last-name'],
					'_wpfdb_meta_email'      => $validated_filelds['feedback-email'],
				),
			);

			$post_id = wp_insert_post( $post_data );

			if ( 0 === $post_id ) {
				// send status error 500.
				wp_send_json_error( null, 500 );
			}

			/**
			 * Action fire after feedback added succesfully,
			 * action send one parametr - ID of a new feedback.
			 *
			 * @since   1.0.0
			 */
			do_action( 'feedback_added', $post_id );

			// send status success.
			wp_send_json_success();

		} else { // the form has an error.

			/**
			 * Action fire if feedback didn't to add.
			 *
			 * @since   1.0.0
			 */
			do_action( 'feedback_not_added' );
			wp_send_json_error( $validated_filelds->errors, 200 );
		}
	}
}
