<?php
/**
 * Define the internationalization functionality
 *
 * @since       1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Define the internationalization functionality.
 *
 * @since       1.0.0
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Feedback_I18n {

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'wp-feedback', false, dirname( plugin_basename( WPFDB_PLUGIN_FILE ) ) . '/languages' );
	}
}
