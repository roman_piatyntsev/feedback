<?php
/**
 * The file that manage metabox
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The class that manage metabox
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */
class WPFDB_Feedback_Metabox {

	/**
	 * Set up and add the meta box.
	 */
	public function add() {
		add_meta_box(
			'wpfdb_box_meta',
			esc_html__( 'User Meta Data', 'wp-feedback' ),
			array( $this, 'html' ),
			'feedback'
		);
	}

	/**
	 * Save the meta box fields.
	 *
	 * @since 1.0.0
	 * @param int $post_id  The post ID.
	 */
	public function save( $post_id ) {
		if ( array_key_exists( 'feedback-first-name', $_POST ) ) {
			update_post_meta( $post_id, '_wpfdb_meta_first_name', sanitize_text_field( $_POST['feedback-first-name'] ) );
		}

		if ( array_key_exists( 'feedback-last-name', $_POST ) ) {
			update_post_meta( $post_id, '_wpfdb_meta_last_name', sanitize_text_field( $_POST['feedback-last-name'] ) );
		}

		if ( array_key_exists( 'feedback-email', $_POST ) ) {
			update_post_meta( $post_id, '_wpfdb_meta_email', sanitize_email( $_POST['feedback-email'] ) );
		}
	}

	/**
	 * Display the meta box HTML to the user.
	 *
	 * @since 1.0.0
	 * @param WP_Post $post   Post object.
	 */
	public function html( $post ) {
		$first_name = get_post_meta( $post->ID, '_wpfdb_meta_first_name', true );
		$last_name  = get_post_meta( $post->ID, '_wpfdb_meta_last_name', true );
		$email      = get_post_meta( $post->ID, '_wpfdb_meta_email', true );

		/**
		 * Require user meta template.
		 * Use on edit feedback window.
		 */
		require WPFDB_PLUGIN_PATH . 'admin/templates/template-feedback-usermeta.php';
	}
}
