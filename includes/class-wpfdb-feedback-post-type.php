<?php
/**
 * Register custom post type.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register 'feedback' custom post type.
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Feedback_Post_Type {

	/**
	 * Register core post types.
	 */
	public function register_post_types() {

		register_post_type(
			'feedback',
			array(
				'labels'                => array(
					'name'          => __( 'Feedback', 'wp-feedback' ),
					'menu_name'     => __( 'Feedbacks', 'wp-feedback' ),
					'add_new'       => __( 'Add Feedback', 'wp-feedback' ),
					'add_new_item'  => __( 'Add New Feedback', 'wp-feedback' ),
					'edit_item'     => __( 'Edit Feedback', 'wp-feedback' ),
					'new_item'      => __( 'New Feedback', 'wp-feedback' ),
					'view_item'     => __( 'View Feedback', 'wp-feedback' ),
					'search_items'  => __( 'Seach Feedback', 'wp-feedback' ),
				),
				'public'                => true,
				'show_in_menu'          => true,
				'publicly_queryable'    => false,
				'query_var' 			=> false,
				'exclude_from_search'   => true,
				'supports'              => array( 'title', 'editor' ),
				'has_archive'           => false,
				'menu_icon'             => 'dashicons-feedback',
				'show_in_nav_menus'     => true,
				'rewrite'               => array( 'slug' => 'feedback' ),
				'map_meta_cap'          => true, // allow to create caps automatically.
				'capability_type'       => 'feedback',
			)
		);
	}
}
