<?php
/**
 * Register all shortcode of the plugin.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register all shortcode of the plugin.
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Feedback_Shortcodes_Loader {

	/**
	 * The array of shortcodes name registered with WordPress.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array    $shortcodes_list    The list of shortcodes name registered with WordPress.
	 */
	protected $shortcodes_list;

	/**
	 * Shortcode class handler prefix.
	 * Using generate full class name from shortcode name.
	 *
	 * For example:
	 * $class_prefix = 'WPFDB_Shortcode_';
	 * Shortcode name 'feedback-form' will be transorm to the valid class name 'WPFDB_Shortcode_Feedback_Form'.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $class_prefix    Shortcode class handler prefix.
	 */
	protected $class_prefix;

	/**
	 * Shortcode handler function name.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $shortcode_handler   Shortcode handler function name.
	 */
	protected $shortcode_handler;

	/**
	 * Initialize the instance to manage the shortcodes.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		$this->class_prefix      = 'WPFDB_Shortcode_';
		$this->shortcode_handler = 'shortcode_handler';
		$this->shortcodes_list   = array(
			'feedback-form',
			'feedback-list',
		);
	}

	/**
	 * Add a new shortcode to the collection to be registered with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function add_shortcode() {

		foreach ( $this->shortcodes_list as $shortcode ) {
			/**
			 * Generate class handler name
			 */
			$shortcode_classpart = str_replace( '-', '_', ucwords( $shortcode, '-' ) );
			$class_handler_name  = $this->class_prefix . $shortcode_classpart;

			/**
			 * Check if a shorcode has a handler.
			 */
			if ( class_exists( $class_handler_name, true ) ) {
				add_shortcode( $shortcode, array( new $class_handler_name(), $this->shortcode_handler ) );
			}
		}
	}
}
