<?php
/**
 * Register hooks to activate/deactivate plugin.
 *
 * @since 1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/config
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register hooks to activate/deactivate plugin.
 *
 * @since 1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/config
 */
class WPFDB_Feedback_Install {

	/**
	 * Handler of 'register_activation_hook' function.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		/**
		 * Add capability to admin role
		 */
		WPFDB_Customize_Admin_Role::activate();
	}

	/**
	 * Handler of 'register_deactivation_hook' function.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		/**
		 * Add capability to admin role
		 */
		WPFDB_Customize_Admin_Role::deactivate();
	}
}
