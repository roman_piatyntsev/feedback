<?php
/**
 * Register autoloader of PHP classes
 *
 * @since 1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/config
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register autoloader of PHP classes
 * Load and register spl_autoload_register() function
 * https://www.php.net/manual/ru/function.spl-autoload-register.php
 *
 * @since 1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/config
 */
class WPFDB_Feedback_Auto_Loader {

	/**
	 * The array of dir's PHP classes.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array
	 */
	private $dirs = array();

	/**
	 * Initialization of the autoloader class instance
	 * and registering handler for PHP classes autoload function
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		spl_autoload_register( array( $this, 'loader' ) );
	}

	/**
	 * Add existing plugins PHP classes dirs
	 *
	 * @since    1.0.0
	 */
	public function register() {
		$this->dirs = array(
			WPFDB_PLUGIN_PATH,
			WPFDB_PLUGIN_PATH . 'includes/',
			WPFDB_PLUGIN_PATH . 'includes/config/',
			WPFDB_PLUGIN_PATH . 'includes/shortcodes/',
			WPFDB_PLUGIN_PATH . 'includes/ajax/',
			WPFDB_PLUGIN_PATH . 'public/',
			WPFDB_PLUGIN_PATH . 'admin/',
		);
	}

	/**
	 * Handler of PHP autoload function.
	 *
	 * Convert file name to class name
	 * and if exist require the file.
	 *
	 * @since 1.0.0
	 * @param string $classname Name of needed PHP class.
	 */
	public function loader( $classname ) {
		$classname = strtolower( $classname );
		$classname = str_replace( '_', '-', $classname );
		foreach ( $this->dirs as $dir ) {
			$file = "{$dir}class-{$classname}.php";
			if ( file_exists( $file ) ) {
				require_once $file;
				return;
			}
		}
	}
}

/**
 * Init instance and register PHP plugin's classes autoload
 */
$auto_loader = new WPFDB_Feedback_Auto_Loader();
$auto_loader->register();
