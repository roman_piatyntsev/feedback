<?php
/**
 * Register shortcode [feedback-list] of the plugin.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register [feedback-list] shortcode of the plugin.
 * Shortcode return list of all feedbacks to view only administrator role.
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Shortcode_Feedback_List extends WPFDB_Shortcodes {

	/**
	 * Responsible for the print scripts in footer while on shortcode use.
	 *
	 * @since    1.0.0
	 * @var      bool   $add_script    Script print condition.
	 */
	public static $add_script = false;

	/**
	 * Responsible for render list of all feedbacks.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WPFDB_Feedback_List_Handler    $feedback_list_hundler    The feedback list.
	 */
	protected $feedback_list_hundler;

	/**
	 * Init instance of class.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function __construct() {
		/**
		 * Init form object instance.
		 */
		$this->feedback_list_hundler = WPFDB_Feedback_List_Handler::get_instance();
	}

	/**
	 * Handler of shortcode.
	 * Example: [feedback-list]
	 *
	 * @param array|string $atts Optional. Array of shortcode attributes.
	 * @param string       $content Optional. Content of shortcode [scode]conntent[/scode].
	 * @param string       $name Optional. Name of shorcode.
	 */
	public function shortcode_handler( $atts = '', $content = '', $name ) {

		/**
		 * Turn on flag to print scripts.
		 */
		self::$add_script = true;

		return $this->feedback_list_hundler->print_list();
	}
}
