<?php
/**
 * Register shortcode [feedback-form] of the plugin.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * Register [feedback-form title="your title"] shortcode.
 * Shortcode returns feedback HTML form and enqueue necessary scripts.
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Shortcode_Feedback_Form extends WPFDB_Shortcodes {

	/**
	 * Responsible for the print scripts in footer while on shortcode use.
	 *
	 * @since    1.0.0
	 *
	 * @var      bool   $add_script    Script print condition.
	 */
	public static $add_script = false;

	/**
	 * Responsible for render and validating the form for add a feedback.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WPFDB_Feedback_Form_Handler    $form_hundler    The feedback form instance.
	 */
	protected $form_hundler;

	/**
	 * Init instance of class.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function __construct() {
		/**
		 * Init form object instance.
		 */
		$this->form_hundler = WPFDB_Feedback_Form_Handler::get_instance();
	}

	/**
	 * Handler of shortcode.
	 * Allowed attributes: "title" - title of the form.
	 * Example: [feedback-form title="" thankyou-msg=""]
	 *
	 * @param array|string $atts Optional. Array of shortcode attributes.
	 * @param string       $content Optional. Content of shortcode [scode]conntent[/scode].
	 * @param string       $name Optional. Name of shorcode.
	 */
	public function shortcode_handler( $atts = '', $content = '', $name ) {

		/**
		 * Turn on flag to print scripts.
		 */
		self::$add_script = true;

		$atts = shortcode_atts(
			array(
				'title'        => __( 'Submit your feedback', 'wp-feedback' ),
				'thankyou-msg' => __( 'Thank you for sending us your feedback', 'wp-feedback' ),
			),
			$atts
		);

		$form_data = $this->prepare_form_data( $atts );

		return $this->form_hundler->print_form( $form_data );
	}

	/**
	 * Prepare data to init form.
	 *
	 * @since    1.0.0
	 * @param    array $atts Shortcode attribures.
	 * @access   private
	 */
	private function prepare_form_data( $atts ) {

		/**
		 * Get user meta
		 */
		$form_data = WPFDB_Feedback_User::get_instance()->get_user_data();

		/**
		 * Add shortcode attributes to form data.
		 */
		$form_data['form-title']   = sanitize_text_field( $atts['title'] );
		$form_data['thankyou-msg'] = sanitize_text_field( $atts['thankyou-msg'] );

		return $form_data;
	}
}
