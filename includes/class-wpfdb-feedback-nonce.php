<?php
/**
 * The file that defines the wp-nonce fields class.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The wp-nonce fileds class.
 *
 * This is used to define AJAX wp-nonce fields.
 *
 * @since       1.0.0
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Feedback_Nonce {

	/**
	 * WP Nonce field for add feedback action.
	 *
	 * @since    1.0.0
	 * @var      string $nonce_add_feedback  Add feedback action.
	 */
	public static $nonce_add_feedback = 'nonce_feedback_add';

	/**
	 * WP Nonce field for get feedback action.
	 *
	 * @since    1.0.0
	 * @var      string $nonce_add_feedback  Get feedback action.
	 */
	public static $nonce_get_feedback = 'nonce_feedback_get';

	/**
	 * Generate nonce for feedback form.
	 *
	 * @since 1.0.0
	 */
	public static function the_nonce_add_feedback() {
		wp_nonce_field( self::$nonce_add_feedback, self::$nonce_add_feedback, false, true );
	}

	/**
	 * Verify nonce from feedback form.
	 *
	 * @since 1.0.0
	 */
	public static function verify_nonce_add_feedback() {
		return check_ajax_referer( self::$nonce_add_feedback, self::$nonce_add_feedback, false );
	}

	/**
	 * Generate nonce for feedback list.
	 *
	 * @since 1.0.0
	 */
	public static function get_nonce_get_feedback() {
		return wp_create_nonce( self::$nonce_get_feedback );
	}

	/**
	 * Verify nonce get feedback list.
	 *
	 * @since 1.0.0
	 */
	public static function verify_nonce_get_feedback() {
		return check_ajax_referer( self::$nonce_get_feedback, self::$nonce_get_feedback, false );
	}
}
