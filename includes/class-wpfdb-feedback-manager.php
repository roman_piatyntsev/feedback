<?php
/**
 * Load th plugin. Core class.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/include
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The core plugin class.
 *
 * This is used to define internationalization, ajax, metabox, shortcodes, admin-specific hooks, and
 * public-facing site hooks.
 *
 * @since       1.0.0
 * @package     Feedback
 * @subpackage  Feedback/include
 */
class WPFDB_Feedback_Manager {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WPFDB_Feedback_Hooks_Loader    $hooks_loader    Maintains and registers all hooks for the plugin.
	 */
	protected $hooks_loader;

	/**
	 * The loader that's responsible for maintaining and registering all plugin's shortcodes
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WPFDB_Feedback_Shortcodes_Loader    $shortcodes_loader    Maintains and registers all shortcodes for the plugin.
	 */
	protected $shortcodes_loader;

	/**
	 * Init instance and run core of plugin.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function __construct() {

		$this->hooks_loader = new WPFDB_Feedback_Hooks_Loader();

		/**
		 * Load all registered plugins hooks.
		 */
		$this->add_plugins_hooks();

		/**
		 * Load all registered plugins shortcodes.
		 */
		$this->add_plugins_shortcodes();
	}

	/**
	 * Add and register plugin's hooks
	 *
	 * @since 1.0.0
	 */
	public function add_plugins_hooks() {

		/**
		 * Init current user
		 */
		$this->init_current_user();

		/**
		 * Register plugin's customm post type
		 */
		$this->register_custom_post_types();

		/**
		 * Add AJAX hundlers.
		 */
		$this->add_ajax_hooks();

		/**
		 * Set plugin text domain
		 */
		$this->set_locale();

		/**
		 * Add hooks for public.
		 */
		$this->add_public_hooks();

		/**
		 * Add hooks for admin.
		 */
		$this->add_admin_hooks();

		/**
		 * Add metabox.
		 */
		$this->add_metabox();

		/**
		 * Add template unclude
		 */
		$this->template_include();

		/**
		 * Registration WP actions and filters
		 */
		$this->hooks_loader->run();
	}

	/**
	 * Init current user.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function init_current_user() {
		$current_user = WPFDB_Feedback_User::get_instance();
		$this->hooks_loader->add_action( 'plugins_loaded', $current_user, 'init_current_user' );
	}

	/**
	 * Register custom post type 'feedback'
	 * Additional information see here /includes/class-wpfdb-feedback-post-type.php
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function register_custom_post_types() {
		$post_types = new WPFDB_Feedback_Post_Type();
		$this->hooks_loader->add_action( 'init', $post_types, 'register_post_types' );
	}

	/**
	 * Add AJAX hooks.
	 * Additional information see here /includes/ajax/class-wpfdb-feedback-ajax.php
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function add_ajax_hooks() {

		/**
		 * Register AJAX handlers while use.
		 */
		if ( wp_doing_ajax() ) {
			$ajax_handlers = new WPFDB_Feedback_Ajax();
			$ajax_actions  = $ajax_handlers->get_actions();

			foreach ( $ajax_actions as $action_handler_name ) {
				$this->hooks_loader->add_action( 'wp_ajax_' . $action_handler_name, $ajax_handlers, $action_handler_name );
				$this->hooks_loader->add_action( 'wp_ajax_nopriv_' . $action_handler_name, $ajax_handlers, $action_handler_name );
			}
		}
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new WPFDB_Feedback_I18n();
		$this->hooks_loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}


	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function add_admin_hooks() {
		$plugin_admin = new WPFDB_Feedback_Admin();
		$this->hooks_loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * Additional information see here /public/class-wpfdb-feedback-public.php
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function add_public_hooks() {
		$plugin_public = new WPFDB_Feedback_Public();
		$this->hooks_loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->hooks_loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'register_scripts' );

		$this->hooks_loader->add_action( 'wp_footer', $plugin_public, 'print_scripts' );
	}

	/**
	 * Add and register plugin's shortcodes.
	 *
	 * @since 1.0.0
	 * @access   private
	 */
	private function add_plugins_shortcodes() {
		$this->shortcodes_loader = new WPFDB_Feedback_Shortcodes_Loader();
		$this->shortcodes_loader->add_shortcode();
	}

	/**
	 * Add metabox to 'feedback' custom post type.
	 *
	 * Additional information see here /public/class-wpfdb-feedback-metabox.php
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function add_metabox() {
		$metabox = new WPFDB_Feedback_Metabox();
		$this->hooks_loader->add_action( 'add_meta_boxes', $metabox, 'add' );
		$this->hooks_loader->add_action( 'save_post', $metabox, 'save' );
	}

	/**
	 * Prevent access to the page with a list of feedbacks to an unauthorized user.
	 *
	 * Additional information see here /public/class-wpfdb-feedback-list-hendler.php
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function template_include() {
		$list_handler = WPFDB_Feedback_List_Handler::get_instance();
		$this->hooks_loader->add_filter( 'template_include', $list_handler, 'template_include', 10, 1 );
	}
}
