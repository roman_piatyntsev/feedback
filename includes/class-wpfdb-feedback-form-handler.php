<?php
/**
 * The form validation and manage the form template.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The form validation and manage the form template.
 *
 * Functionality the form to add a feedbeck.
 *
 * @package     Feedback
 * @subpackage  Feedback/public
 */
class WPFDB_Feedback_Form_Handler {

	/**
	 * Hold the form fields error.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WP_Error $form_errors  Contains all errors.
	 */
	private $form_errors;

	/**
	 * Hold the form fields value.
	 * After validation contains valid value.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WP_Error $form_errors  Contains form values.
	 */
	private $form_fields;

	/**
	 * Hold the validate form rules.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      array $validate_rules Validate rules.
	 */
	private $validate_rules;

	/**
	 * Hold the class instance.
	 *
	 * @var WPFDB_Feedback_Form_Handler $instance Class instance.
	 */
	private static $instance = null;

	/**
	 * The constructor is private
	 * to prevent initiation with outer code.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {

		$this->form_errors = new WP_Error();

		$this->form_fields = array(
			'feedback-first-name'   => '',
			'feedback-last-name'    => '',
			'feedback-subject'      => '',
			'feedback-email'        => '',
			'feedback-message'      => '',
		);

		$this->validate_rules = apply_filters(
			'feedback_validate_rules',
			array(
				'feedback-first-name' => array(
					'type'      => 'text',
					'max-len'   => 60,
					'message'   => esc_html__( 'Invalid First Name. Empty or more than 60 characters.', 'wp-feedback' ),
				),
				'feedback-last-name' => array(
					'type'      => 'text',
					'max-len'   => 60,
					'message'   => esc_html__( 'Invalid Last Name. Empty or more than 60 characters.', 'wp-feedback' ),
				),
				'feedback-subject' => array(
					'type'      => 'text',
					'max-len'   => 255,
					'message'   => esc_html__( 'Invalid Subject. Empty or more than 255 characters.', 'wp-feedback' ),
				),
				'feedback-email' => array(
					'type'      => 'email',
					'message'   => esc_html__( 'Invalid Email.', 'wp-feedback' ),
				),
				'feedback-message' => array(
					'type'      => 'text',
					'max-len'   => 5000,
					'message'   => esc_html__( 'Invalid Message. Empty or more than 5000 characters.', 'wp-feedback' ),
				),
			)
		);
	}

	/**
	 * Get instance of class.
	 * Singlton.
	 *
	 * @since    1.0.0
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new WPFDB_Feedback_Form_Handler();
		}
		return self::$instance;
	}

	/**
	 * Get form template with current user meta (if exist).
	 * Render the form to add a new feedback.
	 *
	 * @since    1.0.0
	 * @param    array $args Array of from init data.
	 */
	public function print_form( $args ) {

		/**
		 * Set default values of form fileds.
		 *
		 * These fields will come from current user (if exist)
		 * or set to default.
		 */
		$defaults = array(
			'form-title'    => '',
			'first-name'    => '',
			'last-name'     => '',
			'email'         => '',
			'thankyou-msg'  => '',
		);

		ob_start();

		/**
		 * Init data of the form. Used in form template.
		 * See here /public/templates/template-feedback-form.php
		 */
		$form_data = wp_parse_args( $args, $defaults );

		/**
		 * Require form template.
		 */
		require WPFDB_PLUGIN_PATH . 'public/templates/template-feedback-form.php';
		return ob_get_clean();
	}

	/**
	 * Validate form fields.
	 *
	 * @since    1.0.0
	 */
	public function form_validate() {

		/**
		 * Check wp-nonce.
		 */
		if ( ! WPFDB_Feedback_Nonce::verify_nonce_add_feedback() ) {
			wp_send_json_error( null, 403 );
		}

		/**
		 * Get POST data.
		 */
		$fields_to_validate = wp_parse_args( $_POST, $this->form_fields );

		foreach ( $fields_to_validate as $field_name => $value ) {

			$rule = $this->validate_rules[ $field_name ];

			switch ( $rule['type'] ) {
				case 'text':
					$len     = mb_strlen( $value );
					$max_len = $rule['max-len'];

					/**
					 * Check rules and set error if exist
					 */
					if ( 0 === $len || $len > $max_len ) {
						$this->form_errors->add( $field_name, $rule['message'] );
					} else { // save validate value.
						$this->form_fields[ $field_name ] = sanitize_text_field( $value );
					}
					break;
				case 'email':
					/**
					 * Check rules and set error if exist
					 */
					if ( ! is_email( $value ) ) {
						$this->form_errors->add( $field_name, $rule['message'] );
					} else { // save validate value.
						$this->form_fields[ $field_name ] = $value;
					}
					break;
			}
		}

		if ( $this->form_errors->has_errors() ) {
			return $this->form_errors;
		}

		return $this->form_fields;
	}
}
