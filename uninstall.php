<?php
/**
 * Uninstall the plugin
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback
 */

/**
 * If this file is called directly, abort.
 */
defined( 'WP_UNINSTALL_PLUGIN' ) || exit;

$all_feedback_posts = get_posts(
	array(
		'posts_per_page' => -1,
		'post_type'      => 'feedback',
		'post_status'    => 'any, trash, auto-draft',
	)
);

foreach ( $all_feedback_posts as $feedback ) :
	wp_delete_post( $feedback->ID, true );
endforeach;

wp_cache_flush();
