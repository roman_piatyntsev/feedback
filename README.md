Feedback
========

Leave your feedback! The "Feedback" plugin allows you to receive feedbacks from site visitors and display a list of all feedbacks in the a table using a shortcode.

Getting started
---------------

Install the Feedback plugin through the **Add Plugins** screen (**Plugins > Add New**). After activating the plugin, the **Feedbacks** menu will appear in the left sidebar. 

There are two shortcodes registered for the front part of the site.

To display the feedback form **[ feedback-form title = "" thankyou-msg = "" ]**, you can change the default title of the form
and the "thank you" message by using shortcodeas attributes.

To display list of all feedbacks use **[ feedback-list ]** Only the administrator has access to this content.