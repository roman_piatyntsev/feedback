<?php
/**
 * The user meta template.
 * This template using on feedabck edit window.
 *
 * @package Feedback
 * @subpackage Feedback/admin/template
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

?>

<label for="wpfdb_first_name"><?php esc_html_e( 'First Name', 'wp-feedback' ); ?></label>
<input type="text" id="wpfdb_first_name" required name="feedback-first-name" class="feedback-meta-field feedback-first-name" value="<?php echo esc_attr( $first_name ); ?>">

<label for="wpfdb_last_name"><?php esc_html_e( 'Last Name', 'wp-feedback' ); ?></label>
<input type="text" id="wpfdb_last_name" required name="feedback-last-name" class="feedback-meta-field feedback-last-name" value="<?php echo esc_attr( $last_name ); ?>">

<label for="wpfdb_email"><?php esc_html_e( 'Email', 'wp-feedback' ); ?></label>
<input type="email" id="wpfdb_email" required name="feedback-email" class="feedback-meta-field feedback-email" value="<?php echo esc_attr( $email ); ?>">
