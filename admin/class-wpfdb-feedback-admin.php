<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package     Feedback
 * @subpackage  Feedback/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * @package     Feedback
 * @subpackage  Feedback/admin
 */
class WPFDB_Feedback_Admin {

	/**
	 * Enqueue the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * Enqueue the main styles for the admin-facing side of the site.
		 */
		wp_enqueue_style(
			WPFDB_PLUGIN_NAME,
			plugin_dir_url( __FILE__ ) . 'css/feedback-admin.css',
			array(),
			WPFDB_PLUGIN_VERSION
		);
	}
}
