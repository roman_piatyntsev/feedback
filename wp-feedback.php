<?php
/**
 * Plugin Name: Your Feedbacks
 * Plugin URI: https://github.com/codescreen/CodeScreen_i1qazxdv
 * Description: Leave your feedback! The "Feedback" plugin allows you to receive feedbacks from site visitors and display a list of all feedbacks in the a table using a shortcode.
 * Version: 1.0.0
 * Author: Roman Piatyntsev
 * Author URI: https://github.com/romanpiatyntsev
 * Text Domain: wp-feedback
 * Domain Path: /languages
 * Requires at least: 5.6
 * Requires PHP: 7.4
 *
 * @package Feedback
 */

/**
 * If this file is called directly, abort.
 */
defined( 'ABSPATH' ) || exit;

/**
 * The main file on the plugin
 */
define( 'WPFDB_PLUGIN_FILE', __FILE__ );

/**
 * Current version of the plugin
 */
define( 'WPFDB_PLUGIN_VERSION', '1.0.0' );

/**
 * Defining plugin name based on folder name
 */
define( 'WPFDB_PLUGIN_NAME', dirname( plugin_basename( __FILE__ ) ) );

/**
 * Absolute path to plugin from the root file system (with trailing slash)
 */
define( 'WPFDB_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

/**
 * Registration autoload plugin's PHP classes.
 *
 * @since    1.0.0
 */
require_once WPFDB_PLUGIN_PATH . 'includes/config/class-wpfdb-feedback-auto-loader.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/config/class-wpfdb-feedback-install.php
 */
register_activation_hook( __FILE__, 'WPFDB_Feedback_Install::activate' );

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/config/class-wpfdb-feedback-install.php
 */
register_deactivation_hook( __FILE__, 'WPFDB_Feedback_Install::deactivate' );

/**
 * Init and run plugin's core
 */
function run_wpfdb_feedback() {
	new WPFDB_Feedback_Manager();
}

run_wpfdb_feedback();
