=== Feedback ===
Contributors: Roman Piatyntsev
Tags: form, feedback, ajax, multilingual
Requires at least: 5.7
Tested up to: 5.8
Stable tag: 1.0.0

Feedback plugin.

== Description ==

Leave your feedback! The "Feedback" plugin allows you to receive feedbacks from site visitors and display a list of all feedbacks in the a table using a shortcode.

== Installation ==

1. Upload the entire `feedback` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the **Plugins** screen (**Plugins > Installed Plugins**).

You will find **Feedbacks** menu in your WordPress admin screen.

There are two shortcodes registered for the front part of the site.
To display the feedback form [feedback-form title = "" thankyou-msg = ""], you can change the default title of the form
and the "thank you" message by using shortcodeas attributes.
To display list of all feedbacks use [feedback-list] Only the administrator has access to this content.

== Changelog ==

The plugin version 1.0.0

= 1.0.0 =
